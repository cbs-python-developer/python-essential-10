"""Homework 10 Task 4 implementation."""


from hw101_word_frequency import word_frequency


def print_text_statistic(text: str) -> None:
    """Print unique words in the text,
    the number of all the words in the text,
    and the number of unique words."""

    words = word_frequency(text)
    unique_words = [word for word in words if words[word] == 1]
    unique_words_string = ' '.join(unique_words)
    word_count = len(words)
    unique_count = len(unique_words)
    print(f'Unique words in the text: {unique_words_string}')
    print(f'Number of all the words: {word_count}')
    print(f'Number of unique words: {unique_count}')


if __name__ == '__main__':

    txt1 = ('The quick brown fox jumps over the lazy brown fox.\n'
            'The lazy brown fox sleeps over the all night!\n'
            'Foxes are over ...')
    print_text_statistic(txt1)
