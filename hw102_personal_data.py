"""Homework 10 Task 2 implementation."""

import re
from pathlib import Path
from typing import Optional


def get_birthday(text: str) -> Optional[str]:
    """Extract birthday from the text.
    Convert it into the dd.mm.yyyy format.

    >>> get_birthday('01-01-2001')
    '01.01.2001'
    >>> get_birthday('2-1-2001')
    '02.01.2001'
    >>> get_birthday('11/11/2011.')
    '11.11.2011'
    >>> get_birthday(' 11.1.2001')
    '11.01.2001'
    >>> get_birthday('32/11/2011')
    >>> get_birthday('11.13.2011')
    >>> get_birthday('11.11/2011')
    """
    birthday_pattern = re.compile(
        r'\b(?P<day>0?[1-9]|[12]\d|3[01])(?P<sep>[.\-/])'
        r'(?P<month>0?[1-9]|1[012])(?P=sep)'
        r'(?P<year>\d{4})\b'
    )
    match_obj = birthday_pattern.search(text)
    if match_obj:
        birthday = match_obj.groupdict()
        day = f"{birthday['day']:0>2}"
        month = f"{birthday['month']:0>2}"
        year = birthday['year']
        return f"{day}.{month}.{year}"
    return None


def get_phone_number(text: str) -> Optional[str]:
    """Extract Ukrainian phone number from the text.
    Convert it into Microsoft international format.

    >>> get_phone_number('+380 44 123 45 67 - E.123, international format')
    '+380 (44) 1234567'
    >>> get_phone_number('(044) 123 45 67 - E.123, national format')
    '+380 (44) 1234567'
    >>> get_phone_number('+380 (44) 1234567 - Microsoft, international format')
    '+380 (44) 1234567'
    >>> get_phone_number('+380-44-123-45-67')
    '+380 (44) 1234567'
    >>> get_phone_number('+380441234567')
    '+380 (44) 1234567'
    >>> get_phone_number('+380 (044) 1234567')
    '+380 (44) 1234567'
    >>> get_phone_number('044 1234567')
    '+380 (44) 1234567'
    >>> get_phone_number('44 1234567')
    '+380 (44) 1234567'
    """
    number_pattern = re.compile(
        r'\b(?:380|0|)[(\s\-]?\(?'
        r'(?P<settlement>\d{2})\)?[(\s-]?'
        r'(?P<number123>\d{3})[\s-]?'
        r'(?P<number45>\d{2})[\s-]?'
        r'(?P<number67>\d{2})\b'
    )
    match_obj = number_pattern.search(text)
    if match_obj:
        number = match_obj.groupdict()
        country_code = '380'
        settlement_code = number['settlement']
        phone_number = number['number123'] + number['number45'] + number['number67']
        return f"+{country_code} ({settlement_code}) {phone_number}"
    return None


def get_email(text: str) -> Optional[str]:
    """Extract email from the text.

    >>> get_email('name.surname@domain.net')
    'name.surname@domain.net'
    >>> get_email('user1@example.com')
    'user1@example.com'
    >>> get_email('user@ domain.net')
    >>> get_email('user @domain.net')
    >>> get_email('user@domain.n')
    """
    email_pattern = re.compile(r'\b\S+@\S+\.\S{2,}\b')
    match_obj = email_pattern.search(text)
    if match_obj:
        email = match_obj.group(0)
        return email
    return None


def main(source_file: Path, output_file: Path) -> None:
    """Extract data from a file. Save to another one."""
    with open(source_file, 'rt') as reader, open(output_file, 'wt') as writer:
        for line in reader:
            birthday = get_birthday(line)
            phone_number = get_phone_number(line)
            email = get_email(line)
            writer.write(
                f"Birthday: {birthday} | "
                f"Phone Number: {phone_number} | "
                f"Email: {email}\n"
            )


if __name__ == '__main__':

    data_file = Path('./input_data.txt')
    extracted_data = Path('./extracted_data.txt')
    main(data_file, extracted_data)
