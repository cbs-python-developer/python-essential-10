"""Homework 10 Task 1 implementation."""

import re


def word_frequency(text: str) -> dict[str, int]:
    """Count the number of occurrences of each word in the text."""
    words = re.findall(r'\b(\w+)\b', text)
    return {
        word: words.count(word) for word in set(words)
    }


if __name__ == '__main__':

    txt1 = ('The quick brown fox jumps over the lazy brown fox.\n'
            'The lazy brown fox sleeps over the all night!\n'
            'The fox is over...')
    print(word_frequency(txt1))
