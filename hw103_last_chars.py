"""Homework 10 Task 3 implementation."""

import re


def get_3_last_chars(text: str) -> str:
    """Return the last 3 characters of each word in the string.
    Return the whole word if it consists of less than 3 characters."""
    word_endings = re.findall(r'(\w{1,3})\b', text)
    return " ".join(word_endings)


def main():
    """Main function."""
    answer = input("Enter a string: ")
    print(get_3_last_chars(answer))


if __name__ == '__main__':

    main()
