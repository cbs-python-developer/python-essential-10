"""Homework 10 Task 5 implementation."""

import re
from typing import Optional


def extract_data(text: str) -> Optional[dict]:
    """Extract data from a string."""
    pattern = (
        r"\b(?P<first_name>[А-ЯІЇЄҐ'-]+)\b\W+\b(?P<last_name>[А-ЯІЇЄҐ'-]+)\b\W+"
        r"\b(?P<birthday>(?:0?[1-9]|[12]\d|3[01])[.\-/](?:0?[1-9]|1[012])[.\-/](?:\d{4}))\b\W+"
        r"\b(?P<email>\S+@\S+\.\S{2,})\b\W+(?P<feedback>.+)$"
    )
    match_obj = re.search(pattern, text, re.IGNORECASE)
    if match_obj:
        return match_obj.groupdict()
    return None


def main() -> Optional[dict]:
    """Main function."""
    text = input("Введіть Ваше ім'я, прізвище, дату народження, email та відгук: ")
    return extract_data(text)


if __name__ == "__main__":
    print(main())
